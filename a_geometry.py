import json
import numpy as np
import json
import os
import time
import cv2
import matplotlib
import random
import math


def mat_float32touint8(mat):
    return (mat*255).astype(np.uint8)


def mat_uint8tofloat32(mat):
    return mat.astype(np.float32)*1.0/255.0


def interize(b):
    return (int(b[0]), int(b[1]))


def transformVecBy2x3(input, M):
    thing = [0, 0]
    thing[0] = (input[0] * M[0, 0]) + (input[1] * M[0, 1]) + M[0, 2]
    thing[1] = (input[0] * M[1, 0]) + (input[1] * M[1, 1]) + M[1, 2]

    return thing


def piecewise(x, pts_list):
    def in_line(x, p1, p2):
        return ((p2[1]-p1[1])/(p2[0]-p1[0]))*(x-p1[0])+p1[1]
    for i in range(len(pts_list)):
        if (x > pts_list[i][0]) and (x < pts_list[i+1][0]):
            return in_line(x, pts_list[i], pts_list[i+1])
    raise RuntimeError("Input not in list of points!")


def draw_hand_rainbow_pts(img, pts):
    for j in range(21):
        rgb = matplotlib.colors.hsv_to_rgb([j/21.0, 1.0, 1.0])
        for ele in rgb:
            ele *= 255.0
        cv2.circle(img, (int(pts[j][0]), int(pts[j][1])),
                   5, (rgb[2], rgb[1], rgb[0]), cv2.FILLED)


def calc_num_times_to_make_image(requested_num_images, num_unique_hands):
    base_num = math.floor(requested_num_images / num_unique_hands)
    extra_probability = (requested_num_images %
                         num_unique_hands) / num_unique_hands
    am = base_num
    if (random.random() < extra_probability):
        am += 1
    return am


def npImgWidth(img):
    return img.shape[1]


def npImgHeight(img):
    return img.shape[0]


def normalizeGrayscaleImage(img, report=None,  target_mean=0.5, target_std=0.25):
    if img.dtype == np.uint8:
        img = mat_uint8tofloat32(img)
    std = np.std(img)
    if std < 0.0001:
        if report is not None:
            print(f"Very low contrast: {report}")
        # Ugh
        img = np.random.random(img.shape)
        std = np.std(img)
    # print(np.std(img))
    img *= target_std / std
    img += target_mean - np.mean(img)

    np.clip(img, 0, 1)

    return img


def draw_rectangle_in_image_px_coord(image, top, bottom, left, right, color):
    # width = image.shape[1]
    # height = image.shape[0]
    # left *= width
    # right *= width
    # top *= height
    # bottom *= height
    # I wish I had any idea why the normal cv2.rectangle() crashes my machine.
    # like it legit crashes my GPU and I have to restart my computer.
    cv2.line(image, (int(left), int(top)),
             (int(right), int(top)), color, thickness=1)
    cv2.line(image, (int(right), int(top)),
             (int(right), int(bottom)), color, thickness=1)
    cv2.line(image, (int(right), int(bottom)),
             (int(left), int(bottom)), color, thickness=1)
    cv2.line(image, (int(left), int(bottom)),
             (int(left), int(top)), color, thickness=1)


def draw_square_in_image_px_coord(image, center, square_side, color):
    draw_rectangle_in_image_px_coord(
        image, center[1] - (square_side / 2),
        center[1] + (square_side / 2),
        center[0] - (square_side / 2),
        center[0] + (square_side / 2),
        color)


def draw_rectangle_in_hmap(hmap, top, bottom, left, right, val):
    width = hmap.shape[1]
    height = hmap.shape[0]

    top = int(top * height)
    bottom = int(bottom * height)

    left = int(left * width)
    right = int(right * width)

    hmap[top:bottom, left:right] = val


def darknet_to_square_in_image_px_coord(image_w, image_h, cx, cy, w, h):
    center = (cx*image_w, cy*image_h)
    square_size = max(w*image_w, h*image_h)
    return center[0], center[1], square_size

def map_ranges(value, from_low, from_high, to_low, to_high):
	return (value - from_low) * (to_high - to_low) / (from_high - from_low) + to_low